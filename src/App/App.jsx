import React, { Component } from 'react';
import { Home } from '../pages/Home';
import { NavigationBar } from '../components/NavigationBar';
import { UserInfo } from '../pages/UserInfo';
import { BrowserRouter, Route } from 'react-router-dom'
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { fetchFriends } from '../store/action/friends'
import { getProfile } from '../store/action/getProfile'

class App extends Component {
  UNSAFE_componentWillMount() {
    const token = localStorage.getItem('token');
    this.props.getProfile() // Note: а зачем вызывать тут если у тебя внутри есть проверка на наличие токена?

    if (token) {
      this.props.fetchFriends(0, '')
    }
  }

  render() {
    return (
      <div>
        <BrowserRouter>
          <NavigationBar/>
          <Route path="/" exact component={Home}/>
          <Route path="/userInfo/:id" exact component={UserInfo} />
        </BrowserRouter>
      </div>
    );
  }
}

function mapDispatchToProps(dispatch) {
  return bindActionCreators({ fetchFriends, getProfile }, dispatch)
}

export default connect(null, mapDispatchToProps)(App);
