export const addNewUser = (user) => {
  return {
    type: 'ADD_USER',
    payload: user
  }
}

export const changeSearchString = (string) => {
  return {
    type: 'A_SEARCH', // Note: Вроде везде писалось действие, а тут просто А
    searchString: string.searchString
  }
}
