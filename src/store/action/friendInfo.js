export const FETCH_FRIEND_INFO = 'FETCH_FRIEND_INFO'
export const FETCH_FRIEND_INFO_SUCCESS = 'FETCH_FRIEND_INFO_SUCCESS'
export const FETCH_FRIEND_INFO_FAIL = 'FETCH_FRIEND_INFO_FAIL'
// Note: вообще эти типы лучше вынести в отдельный файл types, и оттуда импортить.

export function fetchFriendInfo(id) {
  return function(dispatch) {
    dispatch({
      type: FETCH_FRIEND_INFO
    })
    //eslint-disable-next-line
    VK.Api.call(
      'users.get',
      {
        extended: 1,
        user_ids: id,
        fields: 'photo_200_orig,city,domain,bdate,relation,interests,status,education',
        v: '5.80'
      },
      r => {
        console.log(r) // Note: лишнее
        if (r.response) {
          let friendInfo = r.response[0]

          dispatch({
            type: FETCH_FRIEND_INFO_SUCCESS,
            payload: friendInfo,
          })
        } else {
          dispatch({
            type: FETCH_FRIEND_INFO_FAIL,
            error: true,
            payload: new Error('Ошибка авторизации'),
          })
        }
      },
    4)
  }
}
