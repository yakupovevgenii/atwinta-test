export const GETPROFILE_REQUEST = 'GETPROFILE_REQUEST' // Note: GET_PROFILE_REQUEST
export const GETPROFILE_SUCCESS = 'GETPROFILE_SUCCESS' // Note: GET_PROFILE_SUCCESS
export const GETPROFILE_FAIL = 'GETPROFILE_FAIL' // Note: GET_PROFILE_REJECT

export function getProfile() {
  return function(dispatch) {
    dispatch({
      type: GETPROFILE_REQUEST,
    })

    //eslint-disable-next-line no-undef
    VK.init(
      {apiId: 6999494}
    );

    const token = localStorage.getItem('token')
    if (token) {
      //eslint-disable-next-line no-undef
      VK.Auth.getLoginStatus((response) => {
        if (response.session) {
          //eslint-disable-next-line no-undef
          VK.Api.call(
            'users.get',
            {user_ids: `${response.session.mid}`, v: '5.80'},
            response => {
              dispatch({
                type: GETPROFILE_SUCCESS,
                payload: response.response[0],
              })
            }
          )
          response.session.mid
        }
      });
    }
  }
}
