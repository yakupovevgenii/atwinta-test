export const LOGIN_REQUEST = 'LOGIN_REQUEST'
export const LOGIN_SUCCESS = 'LOGIN_SUCCESS'
export const LOGIN_FAIL = 'LOGIN_FAIL'
import { fetchFriends } from './friends'

// Note: вот это можно было вынести в отдельную папку-модуль auth.
// И в нее же добавить методы из getProfile.js
export function handleLogin() {
  return function(dispatch) {
    dispatch({
      type: LOGIN_REQUEST,
    })

    //eslint-disable-next-line no-undef
    VK.Auth.login(r => {
      if (r.session) {
        let username = r.session.user
        const token = r.session.sid;
        localStorage.setItem('token', token)
        dispatch({
          type: LOGIN_SUCCESS,
          payload: username,
        })
        dispatch(fetchFriends());

      } else {
        dispatch({
          type: LOGIN_FAIL,
          error: true,
          payload: new Error('Ошибка авторизации'),
        })
      }
    }, 4)

  }
}
