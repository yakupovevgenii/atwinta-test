export const FETCH_FRIENDS = 'FETCH_FRIENDS'
export const FETCH_FRIENDS_SUCCESS = 'FETCH_FRIENDS_SUCCESS'
export const FETCH_FRIENDS_FAIL = 'FETCH_FRIENDS_FAIL'
export const FETCH_FRIENDS_SEARCH_SUCCESS = 'FETCH_FRIENDS_SEARCH_SUCCESS'
export const DELETE_FRIEND_SEARCH = 'DELETE_FRIEND_SEARCH'

export function deleteFriendSearch() {
  return function(dispatch) {
    dispatch({
      type: DELETE_FRIEND_SEARCH,
    })
  }
}

// Note: лучше сделать два отдельных метода.
// Если добавится третье условие "Выводить 5 рандомных пользователей, если ничего не найдено", ты же не будешь добавлять сюда еще одно условие
// 1 метод - 1 запрос
export function fetchFriends(offset, searchString) {
  return function(dispatch) {
    console.log(offset, 'off') // Note: лишнее
    dispatch({
      type: FETCH_FRIENDS,
    })

    if (searchString !== '') {
      console.log(typeof searchString !== 'undefined', searchString, searchString !== '')
      //eslint-disable-next-line no-undef
      VK.Api.call(
        'users.search',
        { extended: 1, from_list: 'friends', q: searchString, fields: 'photo_100,city,domain,contacts', count: 10, offset: offset || 0, v: '5.80' },
        r => {
          if (r.response) {
            let friends = r.response.items

            dispatch({
              type: FETCH_FRIENDS_SEARCH_SUCCESS,
              payload: friends,
            })
          } else {
            dispatch({
              type: FETCH_FRIENDS_FAIL,
              error: true,
              payload: new Error('Ошибка авторизации'),
            })
          }
        }, 4)
      } else {
        //eslint-disable-next-line no-undef
        VK.Api.call(
          'friends.get',
          { extended: 1, count: 10, offset: offset || 0, fields: 'photo_100,city,domain,contacts', v: '5.80' },
          r => {
            if (r.response) {
              let friends = r.response.items

              dispatch({
                type: FETCH_FRIENDS_SUCCESS,
                payload: friends,
              })
            } else {
              dispatch({
                type: FETCH_FRIENDS_FAIL,
                error: true,
                payload: new Error('Ошибка авторизации'),
              })
            }
          }, 4)
      }
  }
}
