import { combineReducers } from 'redux';
import { userReducer } from './userReducer';
import { friends } from './friendsReducer'; // Note: называл бы уже все по шаблону nameReducer
import { friendInfo } from './friendInfoReducer';
import Search from './searchReducer';

// Note: и еще проверь правильность названия папки reduSers

const allReducers = combineReducers({
  user: userReducer,
  search: Search,
  friends: friends,
  friendInfo: friendInfo
})

export default allReducers;
