import { FETCH_FRIEND_INFO, FETCH_FRIEND_INFO_SUCCESS, FETCH_FRIEND_INFO_FAIL } from '../action/friendInfo'
// Note: название папки с большой почему-то
const initialState = {
  friendInfo: {}, // Note: с названиями что-то не так. У тебя модуль называется friendInfo. Зачем внутри него делать поле "friendInfo". Просто fields.
  error: '',
  isFetching: false,
}

export function friendInfo(state = initialState, action) {
  switch (action.type) {
    case FETCH_FRIEND_INFO:
      return { ...state, isFetching: true, error: '' }

    case FETCH_FRIEND_INFO_SUCCESS:
      return { ...state, isFetching: false, friendInfo: action.payload }

    case FETCH_FRIEND_INFO_FAIL:
      return { ...state, isFetching: false, error: action.payload.message }

    default:
      return state
  }
}
