import {
  FETCH_FRIENDS,
  FETCH_FRIENDS_SUCCESS,
  FETCH_FRIENDS_FAIL,
  FETCH_FRIENDS_SEARCH_SUCCESS ,
  DELETE_FRIEND_SEARCH
} from '../action/friends'

const initialState = {
  friends: [],
  error: '',
  isFetching: false,
  friendsSearch: []
}

export function friends(state = initialState, action) {
  switch (action.type) {
    case FETCH_FRIENDS:
      return { ...state, isFetching: true, error: '' }

    case FETCH_FRIENDS_SUCCESS:
      console.log(action.payload, state, 'state') // Note: лишнее
      return {
        ...state,
        isFetching: false,
        friends: state.friends.concat(action.payload)
      }

    case FETCH_FRIENDS_SEARCH_SUCCESS:
      return {
        ...state,
        isFetching: false,
        friendsSearch: action.payload
      }

    case FETCH_FRIENDS_FAIL:
      return { ...state, isFetching: false, error: action.payload.message }

    case DELETE_FRIEND_SEARCH:
      return { ...state, isFetching: false, friendsSearch: [] }

    default:
      return state
  }
}
