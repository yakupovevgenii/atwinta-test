export default function(state = {
  searchString: ''
}, action) {
  if (action.type === 'A_SEARCH') {
    return {
      ...state,
      searchString: action.searchString,
    }
  }
  
  return state 
}