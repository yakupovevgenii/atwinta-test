import { LOGIN_REQUEST, LOGIN_SUCCESS, LOGIN_FAIL } from '../action/UserActions'
import { GETPROFILE_REQUEST, GETPROFILE_SUCCESS, GETPROFILE_FAIL } from '../action/getProfile'

const initialState = {
  user: '',
  error: '',
  isFetching: false,
}

export function userReducer(state = initialState, action) {
  switch (action.type) {
    case LOGIN_REQUEST: 
    case GETPROFILE_REQUEST:
      return { ...state, isFetching: true, error: '' }

    case LOGIN_SUCCESS:
    case GETPROFILE_SUCCESS:
      return { ...state, isFetching: false, user: action.payload }

    case LOGIN_FAIL:
    case GETPROFILE_FAIL:
      return { ...state, isFetching: false, error: action.payload.message }

    default:
      return state
  }
}
