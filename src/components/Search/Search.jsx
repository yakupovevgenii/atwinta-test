import React, { Component } from "react";
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { changeSearchString } from '../../store/action/index'
import './Search.styl'
import { debounce } from 'lodash'
import { deleteFriendSearch } from '../../store/action/friends'

class Search extends Component {

  constructor(props) {
    super(props)
    this.state = { search: '' }
    this.delayedCallback = debounce(this.setSearch, 300)
  }

  setSearch = (event) => {
    // Note: а зачем тут в state записывать переменную, если можно обойтись const { value } = event.target и работать только с ним?
    this.setState({ search: event.target.value })

    this.props.changeSearchString({
      searchString: event.target.value
    })

    if (this.state.search === '') {
      this.props.deleteFriendSearch()
    }
  }

  handleChangeSearch = (event) => {
    event.persist()
    this.delayedCallback(event)
  }

  render() {
    return (
      <div className="search">
        <input className="search__input" placeholder="поиск" onChange={this.handleChangeSearch} />
      </div>
    );
  }
}

function mapStateToProps(state) {
  return {
    search: state.search
  }
}

function matchDispatchToProps(dispatch) {
  return bindActionCreators({ changeSearchString, deleteFriendSearch }, dispatch)
}

export default connect(mapStateToProps, matchDispatchToProps)(Search);
