import * as React from 'react'
import './Button.styl'

// Note: Ее в принципе можно было объединить с ButtonBack. Только по типу их разбить
export const Button = ({ onClick, type, as: Component = 'button', ...rest }) => (
  <Component {...rest} onClick={onClick} className={`button button_${type} ${rest.className}`} />
)
