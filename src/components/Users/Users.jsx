import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Link } from "react-router-dom";
import { Text } from '../Text'
import { bindActionCreators } from 'redux';
import { fetchFriends, deleteFriendSearch } from '../../store/action/friends'
import './Users.styl'

class Users extends Component {

  state = {
    pageUsers: 1, // Note: можно просто page
    userList: [],
    lastPage: 0,
    oldSearch: '',
    pageHeight: 0,
    oldSearchString: ''
  }

  UNSAFE_componentWillUpdate(nextProps) {
    if (nextProps.searchString !== '' &&
    nextProps.searchString !== this.props.searchString) {
      this.props.fetchFriends(0, nextProps.searchString)

      this.setState({
        oldSearchString: nextProps.searchString
      })
    }
  }

  showMoreUser = () => {
    this.setState({
      pageUsers: this.state.pageUsers + 1
    })

    this.props.fetchFriends(this.state.pageUsers * 10, '')
  }

  handleScroll = () => {
    // Note: const { body } = document
    const body = document.body
    const html = document.documentElement;

    const height = Math.max(
      body.scrollHeight,
      body.offsetHeight,
      html.clientHeight,
      html.scrollHeight,
      html.offsetHeight
    );
    const scrollTop = window.pageYOffset || document.documentElement.scrollTop;

    if (scrollTop + document.documentElement.clientHeight >= height ) {
      this.showMoreUser()
    }
  }

  UNSAFE_componentWillMount() {
    this.props.fetchFriends(10, '')
    this.setState({
      pageHeight: window.clientHeight
    })
    window.addEventListener('scroll', this.handleScroll);
  }

  UNSAFE_componentWillUnmount() {
    window.removeEventListener('scroll', this.handleScroll);
  }

  showUserList = () => {
    let index = 0; // Note: где-то есть ";", где-то нет. Настрой линтер
    const userList = []
    let userListFound = [] // Note: const

    console.log(this.props.user.user) // Note: надо убрать
    if (this.props.user.user === '') {
      // Note: Вообще нужно вынести в App. Если я не авторизован, то не должен иметь доступ ко всему сайту (см. скрин, которые я скинул)
      return <Text>Авторизуйтесь для поиска друзей</Text>
    }

    if (this.props.usersSearch.length !== 0) {
      this.props.usersSearch.forEach((user) => {
        userListFound.push(user)
      })
    } else if (this.props.searchString !== '') {
      return <Text>Друзей не найдено</Text>
    } else {
      this.props.users.forEach((user) => {
        userListFound.push(user)
      })
    }

    for (index; index < this.state.pageUsers * 10; index++) {
      const user  = userListFound[index];
      if (typeof user !== 'undefined') {
        userList.push(
          <Link to={`/userInfo/${user.id}`} key={user.id} className="user__link">
            <div className="user" key={user.id}>
              <div className="user__img">
                <img src={user.photo_100} />
              </div>

              <div className="user__info">
                <div className="user__full-name">
                  <div className="user__name">
                    {user.first_name}&nbsp;
                  </div>
                  <div className="user__last-name">
                    {user.last_name}
                  </div>
                </div>

                <div className="user__contacts">
                  {user.city ? user.city.title : 'не указан'}
                </div>
              </div>
            </div>
          </Link>
        )
      }
    }
    return userList
  }

  render() {
    // Note: Можно же все тут отрисовать. Тем более, если вынести проверку на авторизацию.
    // через map можно выводить компоненты по циклу
    return (
      <div className="users">
        {this.showUserList()}

      </div>
    );
  }
}

function mapStateToProps(state) {
  return {
    user: state.user,
    users: state.friends.friends,
    usersSearch: state.friends.friendsSearch,
    searchString: state.search.searchString
  }
}

function mapDispathToProps(dispatch) {
  return bindActionCreators({fetchFriends, deleteFriendSearch}, dispatch)
}
export default connect(mapStateToProps, mapDispathToProps)(Users);
