import React, { Component } from 'react';
import { Search } from '../Search';
import { connect } from 'react-redux';
import { ButtonBack } from '../ButtonBack';
import { Button } from '../Button';
import { bindActionCreators } from 'redux'
import { handleLogin } from '../../store/action/UserActions'

import { withRouter } from "react-router";
import './NavigationBar.styl';

class NavigationBar extends Component {

  // Note: неиспользуемая функция
  showButtonBack = () => {

    if (this.props.location.pathname.match(/userInfo/)) {
      return <div className="navigation-bar__col_left"><ButtonBack> Назад </ButtonBack></div>
    } else {
      // Note: <div className="navigation-bar__col_left" /> - это ж jsx. Тут не совсем реальные теги используются
      return <div className="navigation-bar__col_left"></div>
    }
  }

  render() {
    return (
      <div className="navigation-bar">
        <div className="navigation-bar__header">
          <div className="navigation-bar__col_left">
            {this.props.location.pathname.match(/userInfo/) && <ButtonBack />}
          </div>

          <h1>Поиск пользователей</h1>

          <div className="navigation-bar__col_right">
            {this.props.user
              ? <div className="navigation-bar__name">{this.props.user.first_name}</div>
              : <Button onClick={this.props.handleLogin} type='action'> Войти </Button>
            }
          </div>

        </div>
        <Search />
      </div>
    );
  }
}

function mapStateToProps(state) {
  return {
    user: state.user.user
  }
}

function matchDispatchToProps(dispatch) {
  return bindActionCreators({ handleLogin }, dispatch)
}

export default connect(mapStateToProps, matchDispatchToProps)(withRouter(NavigationBar));
