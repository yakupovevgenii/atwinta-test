import React, { Component } from 'react'; // Note: неиспользуемые переменные
import { createBrowserHistory } from 'history';
import './ButtonBack.styl'

const history = createBrowserHistory();

export const ButtonBack = () => (
  <button onClick={history.goBack} className="button-back">Назад</button>
)
