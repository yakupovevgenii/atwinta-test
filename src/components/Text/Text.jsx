import React from 'react';
import './Text.styl';

export const Text = ({ type, as: Component = 'span', ...rest }) => (
  <Component {...rest} className={`text text_${type} ${rest.className}`} />
)