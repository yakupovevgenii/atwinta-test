import React, { Component } from 'react';
import { find } from 'lodash';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { fetchFriendInfo } from '../../store/action/friendInfo'
import './UserInfo.styl'

class UserInfo extends Component {

  // Note: а state.user разве используется?
  state = {
    user: find(this.props.users,
      (o) => {
        return o.id === Number(this.props.match.params.id)
      }
    )
  }

  UNSAFE_componentWillMount() {
    console.log('asd') // Note: лишнее
    this.props.fetchFriendInfo(this.props.match.params.id)
  }

  render() {
    return (
      <div className="user-info">
        <div className="user-info__avatar">
          <img src={this.props.info.photo_200_orig} />
        </div>

        <div className="user-info__content">
          <div className="user-info__name">
            <div>{this.props.info.first_name}&nbsp;{this.props.info.last_name}</div>
          </div>

          <div className="user-info__about">
            <div className="user-info__about-row">
              <div className="user-info__about-label">
                День рождения:
              </div>
              <div className="user-info__about-value">
                {this.props.info.bdate}
              </div>
            </div>
            <div className="user-info__about-row">
              <div className="user-info__about-label">
                Город:
              </div>
              <div className="user-info__about-value">
                {this.props.info.city ? this.props.info.city.title : ' не указан'}
              </div>
            </div>
            <div className="user-info__about-row">
              <div className="user-info__about-label">
                Образование:
              </div>
              <div className="user-info__about-value">
                {this.props.info.university_name || 'Не указано'}
              </div>
            </div>
            <div className="user-info__about-row">
              <div className="user-info__about-label">
                Интересы:
              </div>
              <div className="user-info__about-value">
                {this.props.info.interests || 'Не указано'}
              </div>
            </div>

          </div>

          {/*// Note: лишнее? */}
          <div className="user-info__main">
            <div className="user-info__city">
            </div>

            <div className="user-info__sister">
            </div>

          </div>
        </div>
      </div>
    );
  }
}

function mapStateToProps(state) {
  return {
    users: state.friends.friends,
    info: state.friendInfo.friendInfo
  }
}

function mapDispatchToProps(dispatch) {
  return bindActionCreators({ fetchFriendInfo }, dispatch)
}

export default connect(mapStateToProps, mapDispatchToProps)(UserInfo);
