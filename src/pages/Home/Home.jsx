import React, { Component } from 'react';
import { Users } from '../../components/Users';
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux';
import { handleLogin } from '../../store/action/UserActions'  // Note: что-то с большой буквы называется
import { fetchFriends } from '../../store/action/friends'     // Note: ,что-то нет
import './Home.styl'

class App extends Component {

  login = () => {
    this.props.handleLogin()
  }

  render() {

    return (
      <div className="home">
        <Users />
      </div>
    );
  }
}

function matchDispatchToProps(dispatch) {
  return bindActionCreators({ handleLogin, fetchFriends }, dispatch)
}
export default connect(null, matchDispatchToProps)(App);
