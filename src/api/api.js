import axios from 'axios';

export const api = axios.create({
  baseURL: process.env.NODE_ENV === 'development' ? 'https://api.vk.com/method/' : 'https://api.vk.com/method/',
  headers: {
    Accept: 'application/json',
    'X-Requested-With': 'XMLHttpRequest',
    'Content-Type': 'application/jsonp'
  },
  dataType: 'JSON',
  withCredentials: true
});

api.interceptors.request.use(
  config => config,
  // eslint-disable-next-line
  reason => Promise.reject(reason)
);
