import React from 'react';
import ReactDOM from 'react-dom';
import App from './App/App';
import allReducers from './store/Reducer';
import logger from 'redux-logger'
import thunk from 'redux-thunk'
import { createStore, applyMiddleware } from 'redux';
import { Provider } from 'react-redux';
import { BrowserRouter } from "react-router-dom";
import { ping } from './store/enhancers/ping'
import { composeWithDevTools } from 'redux-devtools-extension';

const store = createStore(allReducers, composeWithDevTools(applyMiddleware(logger, thunk, ping)));

ReactDOM.render(
  <BrowserRouter>
    <Provider store={store}> <App /> </Provider>
  </BrowserRouter>,
  document.getElementById('root')
);
